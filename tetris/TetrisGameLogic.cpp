//TetrisGameLogic.cpp
//TODO const 
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include "TetrisGameLogic.h"
#include "TetrisUserInput.h"
#include "TetrisRenderer.h"
#include "TetrisRenderer.h"

using namespace std;


//
TetrisGameLogic::TetrisGameLogic()
{
    //init m_gameGrid   
    m_gameGrid = new char*[ROWSIZE];
    for( int i=0; i<ROWSIZE; ++i)
    {
        m_gameGrid[i] = new char[COLSIZE];
    }  
    initGameGrid();  

    for( int i=0; i<PIECECOORD; ++i)
    {
        m_pieceCoord[i] = 0;
        m_tempPieceCoord[i] = 0;
    }  
    initMatrix();
}

//
TetrisGameLogic::~TetrisGameLogic()
{
    //delete m_gameGrid memory
    for( int i=0; i<ROWSIZE; ++i)
    {
        delete[] m_gameGrid[i];  
    }
    delete[] m_gameGrid;

    //delete pieceCoord memory
    delete[] m_pieceCoord; 
}

//
void TetrisGameLogic::initMatrix()
{
    m_transformationMatrix[0][0] = 0;
    m_transformationMatrix[0][1] = 1;
    m_transformationMatrix[1][0] = -1;
    m_transformationMatrix[1][1] = 0;
}

//
int TetrisGameLogic::randGamePiece()
{
    //rand generate piece 1-7
    srand(time(NULL));
    int piece = (rand() % 7) + 1;
    return piece;
}

//
void TetrisGameLogic::setRenderer(class TetrisRenderer& myConsoleRenderer)
{
        m_renderer = &myConsoleRenderer; 
}

//
void TetrisGameLogic::setUserInput(class TetrisUserInput& myConsloleUserInput)
{
        m_userInput = &myConsloleUserInput; 
}

//used for initial start location of a piece
//pivot  m_pieceCoord elemnt 6&7
// rotates the point matrix 90 degrees clockwise
void TetrisGameLogic::rotatePieceinGameGrid()
{
    int row, col;
    m_transformationMatrix; 
  
    m_pivotRow = m_pieceCoord[6];
    m_pivotCol = m_pieceCoord[7];
    int rowIter = 0;
    int colIter = 1;
    int rowPivotIndex = (PIECECOORD -2);

    //iter thru the pieceCoord arr
    while(rowIter < rowPivotIndex)
    {
        m_pointMatrixRow = m_pieceCoord[rowIter]- m_pivotRow;
        m_pointMatrixCol = m_pieceCoord[colIter]- m_pivotCol;

        if(!multiplyMatrices(rowIter))
        {
            return;
        }

        rowIter += 2;
        colIter += 2;
    }
    copyTempCoord();
}

//
bool TetrisGameLogic::multiplyMatrices(int row)
{
    //transforamtion matrix * pivot matrix
    int newPointMatrixRow = (m_pointMatrixRow * m_transformationMatrix[0][0]) + 
        (m_pointMatrixCol * m_transformationMatrix[0][1]);//Vr
    int newPointMatrixCol = (m_pointMatrixRow * m_transformationMatrix[1][0]) + 
        (m_pointMatrixCol * m_transformationMatrix[1][1]);
    int rowCell = newPointMatrixRow +  m_pivotRow;
    int colCell = newPointMatrixCol +  m_pivotCol;
    bool inBound = isPieceInBounds();
    bool isEmpty = isGridLocationEmpty( );

    if(rowCell < 0 || rowCell > ROWSIZE) 
    {
        return false;
    }
    if(colCell < 0 || colCell > COLSIZE)
    {
        return false;
    }
    m_tempPieceCoord[row] = rowCell;
    m_tempPieceCoord[row + 1] = colCell;
    return true;
}

//
void TetrisGameLogic::initGameGrid()
{ 
    for(int i = 0; i < ROWSIZE; i++)
    {
        for(int j = 0; j < COLSIZE; j++)
        {
            m_gameGrid[i][j] = ' ';
        }
    }
}

//
char TetrisGameLogic::getGameGridElem(int row, int col)
{
      return m_gameGrid[row][col];
}

//row/col entry 6 and 7 are pivot
void TetrisGameLogic::initPieceCoord()
{
    int type = randGamePiece();
    int col = 4;
//cout << "type: " << type << endl;

    //
    if(type == 1)
    {
        //initialize bar: I
        m_pieceCoord[0] = 0;
        m_pieceCoord[1] = 3;
        m_pieceCoord[2] = 0;
        m_pieceCoord[3] = 4;
        m_pieceCoord[4] = 0;
        m_pieceCoord[5] = 6;
        m_pieceCoord[6] = 0;
        m_pieceCoord[7] = 5;
        m_pieceType = 'I';  
    }
 
    // Left L: X
    if(type == 2)
    {
        m_pieceCoord[0] = 0;
        m_pieceCoord[1] = 5;
        m_pieceCoord[2] = 0;
        m_pieceCoord[3] = 6;
        m_pieceCoord[4] = 1;
        m_pieceCoord[5] = 4;
        m_pieceCoord[6] = 0;
        m_pieceCoord[7] = 4;
        m_pieceType = 'X';  
    }

    //Right L: #
    if(type == 3)
    {
        m_pieceCoord[0] = 0;
        m_pieceCoord[1] = 4;
        m_pieceCoord[2] = 0;
        m_pieceCoord[3] = 5;
        m_pieceCoord[4] = 1;
        m_pieceCoord[5] = 6;
        m_pieceCoord[6] = 0;
        m_pieceCoord[7] = 6;
        m_pieceType = '#'; 
    }

    //reverse z: $
    if(type == 4)
    {
        m_pieceCoord[0] = 0;
        m_pieceCoord[1] = 4;
        m_pieceCoord[2] = 1;
        m_pieceCoord[3] = 5;
        m_pieceCoord[4] = 1;
        m_pieceCoord[5] = 6;
        m_pieceCoord[6] = 0;
        m_pieceCoord[7] = 5;
        m_pieceType = '$';  
    }

    //Z: Z
    if(type == 5)
    {
        m_pieceCoord[0] = 0;
        m_pieceCoord[1] = 6;
        m_pieceCoord[2] = 0;
        m_pieceCoord[3] = 5;
        m_pieceCoord[4] = 1;
        m_pieceCoord[5] = 4;
        m_pieceCoord[6] = 1;
        m_pieceCoord[7] = 5;
        m_pieceType = 'Z';  
    }

    //block: &
    if(type == 6)
    {
        m_pieceCoord[0] = 0;
        m_pieceCoord[1] = 4;
        m_pieceCoord[2] = 0;
        m_pieceCoord[3] = 5;
        m_pieceCoord[4] = 1;
        m_pieceCoord[5] = 4;
        m_pieceCoord[6] = 1;
        m_pieceCoord[7] = 5;  
        m_pieceType = '&';       
    }

    //T: A 
    if(type == 7)
    {
        m_pieceCoord[0] = 0;
        m_pieceCoord[1] = 4;
        m_pieceCoord[2] = 0;
        m_pieceCoord[3] = 5;
        m_pieceCoord[4] = 0;
        m_pieceCoord[5] = 6;
        m_pieceCoord[6] = 1;
        m_pieceCoord[7] = 5; 
        m_pieceType = 'A';      
    }  
}

//copy pieceLocation into temp array
void TetrisGameLogic::copyPieceCoord()
{
    for( int i=0; i<PIECECOORD; ++i)
    {
        m_tempPieceCoord[i] = m_pieceCoord[i];
    }  
}


//copy tempLocation into piece array
void TetrisGameLogic::copyTempCoord()
{
    for( int i=0; i<PIECECOORD; ++i)
    {
        m_pieceCoord[i] = m_tempPieceCoord[i];
    }  
}

//moves piece vertically down by one row
void TetrisGameLogic::verticalPieceMove()
{
cout << "verticalPieceMove" << endl;
    int pieceRow;
    int validCount = 0;
    copyPieceCoord();
    if(verticalInBoundsCheck())
    {
        for(int i = 0; i < PIECECOORD; i += 2)
        {
            pieceRow = m_tempPieceCoord[i];
            m_tempPieceCoord[i] = ++pieceRow;
        }
      
        bool inBound = isPieceInBounds();
        bool isEmpty = isGridLocationEmpty( );
      
        if(inBound == 1 && isEmpty == 1)
        {
            deleteCurrPieceInGrid();
            copyTempCoord();
        }
            system("clear");
            usleep(11000);
    }
}

//returns true if in bounds
bool TetrisGameLogic::verticalInBoundsCheck()
{
cout << "verticalInBoundsCheck" << endl;
    int pieceRow;
    for(int i = 0; i < PIECECOORD; i += 2)
    {
        pieceRow = m_tempPieceCoord[i];
        pieceRow++;

        if(!(pieceRow < ROWSIZE))
        {
            return false;
        }
    }
    return true;   
}

//TODO use temp array
//returns true if piece in bounds
bool TetrisGameLogic::isPieceInBounds()
{
    int pieceRow = 0;
    int pieceCol = 1;
    while(pieceRow < PIECECOORD)
    {
        if(m_tempPieceCoord[pieceRow] < 0 || m_tempPieceCoord[pieceRow] >= ROWSIZE)
        {
            return false;
        }
        if(m_tempPieceCoord[pieceCol] < 0 || m_tempPieceCoord[pieceCol] >= COLSIZE)
        {
            return false;
        }
        pieceRow += 2;
        pieceCol += 2;
    }
    return true;
}

const bool TetrisGameLogic::isGridLocationEmpty( )
{
cout << "isGridLocationEmpty" << endl;
    int pieceRow = 0;
    int pieceCol = 1;
    deleteCurrPieceInGrid();//removeCurrPieceFromGrid

    while(pieceRow < PIECECOORD)
    {
        if(m_gameGrid[m_tempPieceCoord[pieceRow]][m_tempPieceCoord[pieceCol]] != ' ')
        {
            undoDeletePiece();
            return false;
        }
        pieceRow += 2;
        pieceCol += 2;
    }
    return true;
}

//m_piece type as param
//set piece in grid for printing
void TetrisGameLogic::setPieceforPrint()
{
    int pieceRow = 0;
    int pieceCol = 1;
    while(pieceRow < PIECECOORD)
    {
        m_gameGrid[m_pieceCoord[pieceRow]][m_pieceCoord[pieceCol]] = m_pieceType;
        pieceRow += 2;
        pieceCol +=2;
    }
}

//remove current piece from grid 
void TetrisGameLogic::deleteCurrPieceInGrid()
{
cout << "deleteCurrPieceInGrid" << endl;
    int pieceRow = 0;
    int pieceCol = 1;
    while(pieceRow < PIECECOORD)
    {
cout << "delete row: " << pieceRow << "  delete col: " << pieceCol << endl;
cout << "piece row " << m_pieceCoord[pieceRow] << "piece col " << m_pieceCoord[pieceCol] << endl;
cout << " game grid " << m_gameGrid[m_pieceCoord[pieceRow]][m_pieceCoord[pieceCol]] << endl;
 
        m_gameGrid[m_pieceCoord[pieceRow]][m_pieceCoord[pieceCol]] = ' ';
        pieceRow += 2;
        pieceCol +=2;
    }
cout << "deleteCurrPieceInGrid EXIT LOOP" << endl;
    return;
}

//undo deleteCurrPieceInGrid and place current piece in grid 
void TetrisGameLogic::undoDeletePiece()
{
cout << "undoDeltetePiece" << endl;
    int pieceRow = 0;
    int pieceCol = 1;

    for(int i = 0; i < PIECECOORD; i += 2)
    {
        int rowData = m_tempPieceCoord[i];
        m_tempPieceCoord[i] = --rowData;
    }

    while(pieceRow < PIECECOORD)
    {
        m_gameGrid[m_pieceCoord[pieceRow]][m_pieceCoord[pieceCol]] = m_pieceType;
        pieceRow += 2;
        pieceCol +=2;
    }
}

void TetrisGameLogic::getTerminalInput()
{

    int input = m_userInput->getUserInput();

    if(input == -1)
    {
    }
    if(input == 0)
    {
        cout << "LEFT" << endl;
        rotatePieceinGameGrid(); 
    }
}

//TODO fix memory error for out of bounds
void TetrisGameLogic::gameLoop()
{
    int count = 0;
    unsigned int time = 150000;
 
    initGameGrid();
    initPieceCoord();
    deleteCurrPieceInGrid();
    m_renderer->printGrid();

//cout << "get user input called" << endl;
       // getTerminalInput();
    while(count < 2)
    {
        usleep(time);
        verticalPieceMove();
        //getTerminalInput(); 
        //rotatePieceinGameGrid();
        m_renderer->printGrid();
        //getTerminalInput();
       //rotatePieceinGameGrid(); 
        //m_renderer->printGrid();
   
        count++; 
    }
m_renderer->printGrid();
    return;
    
}






