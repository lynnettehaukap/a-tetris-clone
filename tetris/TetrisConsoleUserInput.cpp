//

#include <iostream>
#include <stdlib.h>
#include <SDL.h>

#include "TetrisConsoleUserInput.h"
#include "TetrisGameLogic.h"
#define MOVE_SPEED 100.0f
int SDL_Init(Uint32 flags);
SDL_Surface *SDL_SetVideoMode(int width, int height, int bpp, Uint32 flags);

using namespace std;

TetrisConsoleUserInput::TetrisConsoleUserInput(TetrisGameLogic& gameLogic) : TetrisUserInput(gameLogic)
{}

enum KeyPressSurfaces { KEY_PRESS_SURFACE_DEFAULT, KEY_PRESS_SURFACE_UP, KEY_PRESS_SURFACE_DOWN, KEY_PRESS_SURFACE_LEFT, KEY_PRESS_SURFACE_RIGHT, KEY_PRESS_SURFACE_TOTAL };

//get arrowkey input from console
int TetrisConsoleUserInput::getUserInput()
{
cout << "getUserInput" << endl;

    int terminalInput = -1;
    // Initialize the SDL
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        cerr << "SDL_Init() Failed: " <<
        SDL_GetError() << endl;
        exit(1);
    }

    // Set the video mode
    SDL_Surface* display;
    SDL_putenv ("SDL_VIDEODRIVER=dummy"); 
    SDL_SetVideoMode( 320, 200, 0, 0 );
    //display = SDL_SetVideoMode(640, 480, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    if (display == NULL)
    {
        cerr << "SDL_SetVideoMode() Failed: " <<        
        SDL_GetError() << endl;
        exit(1);
    }

    // Gain access to keystate array
    Uint8 *keys = SDL_GetKeyState(NULL);

    // Timing variables
    Uint32 old_time, current_time;
    float ftime;

    // Need to initialize this here for event loop to work
    current_time = SDL_GetTicks();
    SDL_Event event;
    int quit = 1;

    // Box
    float x = 35.0f, y = 35.0f;
    SDL_Rect rect;
    rect.h = 30;
    rect.w = 30;

    // Main loop
    while(quit)
    {
        // Update the timing information
        old_time = current_time;
        current_time = SDL_GetTicks();
        ftime = (current_time - old_time) / 1000.0f;

        // Check for messages
        if (SDL_PollEvent(&event))
        {
            // Check for the quit message
            if (event.type == SDL_QUIT)
            {
            // Quit the program
            break;
            }
        }

        // Handle input
        if (keys[SDLK_LEFT])
        {
            cout << "        MOMMMMMMAAA" << endl;
            terminalInput = 0;
            quit = 0;
        }
        if (keys[SDLK_RIGHT])
         x += MOVE_SPEED * ftime;
        if (keys[SDLK_DOWN])
         y += MOVE_SPEED * ftime;
        if (keys[SDLK_UP])
         y -= MOVE_SPEED * ftime;
    }

    // Tell the SDL to clean up and shut down
    SDL_Quit();
    return terminalInput;
}
    /*char input = cin.get();
    if(input == 'w')
    {
        cout << "rotate" << endl;
        m_gameLogic.rotatePieceinGameGrid(); 
    }
*/




