//
//

#include <iostream>
#include <stdlib.h>
#include "TetrisRenderer.h"
#ifndef TETRIS_CONSOLE_RENDERER_H
#define TETRIS_CONSOLE_RENDERER_H
using namespace std;



class TetrisConsoleRenderer : public TetrisRenderer
{

public:
   TetrisConsoleRenderer( class TetrisGameLogic& gameLogic );
   void printGrid();

};

#endif
