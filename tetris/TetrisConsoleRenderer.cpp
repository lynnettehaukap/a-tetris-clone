//TetrisConsoleRenderer

#include <iostream>
#include <stdlib.h>
#include "TetrisConsoleRenderer.h"
#include "TetrisGameLogic.h"


using namespace std;

TetrisConsoleRenderer::TetrisConsoleRenderer( TetrisGameLogic& game) : TetrisRenderer(game)
{}


void TetrisConsoleRenderer::printGrid()
{
    cout << endl << endl << "                  TETRIS " << endl;
    m_gameLogic.deleteCurrPieceInGrid();
    m_gameLogic.setPieceforPrint();

    cout << "_____________________________________________" <<  endl << endl;
    for(int i = 0; i < ROWSIZE; i++)
    {
        cout <<  "= ";
        for(int j = 0; j < COLSIZE; j++)
        {

            cout << m_gameLogic.getGameGridElem(i, j);
            cout << " | ";
        }
        cout <<  "  =";
        cout << endl;
        }
    cout << "_____________________________________________" << endl << endl << endl;
}
