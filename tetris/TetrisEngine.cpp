//TetrisEngine.cpp
//includes main

#include<iostream>
#include <stdlib.h>
#include "TetrisGameLogic.h"
#include "TetrisConsoleUserInput.h"
#include "TetrisConsoleRenderer.h"

using namespace std;


int main() {

    TetrisGameLogic myLogic;
    TetrisConsoleUserInput myConsoleInput(myLogic);
    TetrisConsoleRenderer myConsoleRenderer(myLogic);
    myLogic.setRenderer(myConsoleRenderer);
    myLogic.setUserInput(myConsoleInput);
    myLogic.gameLoop();
    exit(0);
   // return(0);
}



